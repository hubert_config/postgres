CREATE USER gitlab PASSWORD '<gitlab_password>' CREATEDB;
CREATE DATABASE gitlabhq_production;
ALTER ROLE gitlab WITH LOGIN;
GRANT ALL ON DATABASE gitlabhq_production TO gitlab;
-- Switch to gitlabhq_production
CREATE EXTENSION pg_trgm;
CREATE EXTENSION postgres_fdw;